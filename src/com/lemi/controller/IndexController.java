package com.lemi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

	@RequestMapping("hello")
	public String index(Model model,String name) {
		
		System.out.println("IndexController hello...............");
		
		User user = new User();
		user.setName("liujie");
		user.setTel("18762038071");
		user.setAge(28);
		user.setStatus("启用");
		
		model.addAttribute("user",user);
		model.addAttribute("readField","$status");
		return "hello";
	}
}
